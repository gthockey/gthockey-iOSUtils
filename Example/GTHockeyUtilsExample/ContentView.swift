//
//  ContentView.swift
//  GTHockeyUtilsExample
//
//  Created by Caleb Rudnicki on 1/24/22.
//

import SwiftUI

struct ContentView: View {
    
    init() {
//        Font.loadCustomFonts()
    }
    
    var body: some View {
        NavigationView {
            List(MenuOption.allCases, id: \.self) { option in
                NavigationLink(destination: option.destination) {
                    Text(option.displayName)
                }
            }
            .navigationTitle("GTHockeyUtils")
        }
    }
}

struct ContentView_Previews: PreviewProvider {
    static var previews: some View {
        ContentView()
    }
}
