//
//  MenuOption.swift
//  GTHockeyUtilsExample
//
//  Created by Caleb Rudnicki on 1/24/22.
//

import Foundation
import SwiftUI
import GTHockeyUtils

enum MenuOption: String, CaseIterable, Hashable {
    
    case colors
    case fonts
    case icons
    case imageCarousel
    case htmlText
    case tableView
    case singleColumnGrid
    case doubleColumnGrid
    
    /// The name to be shown in the menu
    var displayName: String {
        switch self {
        case .colors: return "Colors"
        case .fonts: return "Fonts"
        case .icons: return "Icons"
        case .imageCarousel: return "Image Carousel"
        case .htmlText: return "HTML Text"
        case .tableView: return "Table View"
        case .singleColumnGrid: return "Single Column Grid"
        case .doubleColumnGrid: return "Double Column Grid"
        }
    }
    
    /// The `View` that the user will be brought to if the option is selected.
    var destination: AnyView {
        switch self {
        case .colors: return AnyView(ColorsView())
        case .fonts: return AnyView(FontsView())
        case .icons: return AnyView(IconsView())
        case .imageCarousel: return AnyView(ImageCarouselView())
        case .htmlText: return AnyView(HTMLView())
        case .tableView: return AnyView(TableView())
        case .singleColumnGrid: return AnyView(SingleColumnGridView())
        case .doubleColumnGrid: return AnyView(DoubleColumnGridView())
        }
    }
    
}
