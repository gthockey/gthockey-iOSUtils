//
//  ColorsView.swift
//  GTHockeyUtilsExample
//
//  Created by Caleb Rudnicki on 1/24/22.
//

import SwiftUI
import GTHockeyUtils

struct ColorsView: View {
    
    var body: some View {
        ScrollView {
            VStack {
                Text("Background")
                Rectangle()
                    .fill(Color.background)
                    .frame(height: 200)
            }
            
            VStack {
                Text("Secondary Background")
                Rectangle()
                    .fill(Color.secondaryBackground)
                    .frame(height: 200)
            }
            
            VStack {
                Text("Text")
                Rectangle()
                    .fill(Color.text)
                    .frame(height: 200)
            }
            
            VStack {
                Text("Gold")
                Rectangle()
                    .fill(Color.gold)
                    .frame(height: 200)
            }
            
            VStack {
                Text("Navy")
                Rectangle()
                    .fill(Color.navy)
                    .frame(height: 200)
            }
            
            VStack {
                Text("Powder")
                Rectangle()
                    .fill(Color.powder)
                    .frame(height: 200)
            }
            
            VStack {
                Text("Red")
                Rectangle()
                    .fill(Color.destructive)
                    .frame(height: 200)
            }
        }
    }
}

struct ColorsView_Previews: PreviewProvider {
    static var previews: some View {
        ColorsView()
    }
}
