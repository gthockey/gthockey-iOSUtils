//
//  DoubleColumnGridView.swift
//  GTHockeyUtilsExample
//
//  Created by Caleb Rudnicki on 2/18/22.
//

import SwiftUI
import GTHockeyUtils

struct DoubleColumnGridView: View {
    
    private let layout = [
        GridItem(.flexible()),
        GridItem(.flexible())
    ]
    
    var body: some View {
        LazyVGrid(columns: layout, spacing: 20) {
            CustomGridCell() {
                Text("Simple with text")
            }
            
            CustomGridCell() {
                VStack {
                    Text("Simple with text")
                    Text("Simple with text")
                }
            }
            
            CustomGridCell() {
                HStack(alignment: .top, spacing: 10) {
                    Image.buzz
                        .resizable()
                        .scaledToFit()
                        .frame(width: 120, height: 100)
                    VStack(alignment: .leading, spacing: 5) {
                        Text("Fall Recap - Jackets get ready for Nationals Push")
                            .foregroundColor(.text)
                            .font(.normalTextSemibold)
                            .lineLimit(3)
                        Text("Jan 4, 2022")
                            .foregroundColor(.text)
                            .textCase(.uppercase)
                            .font(.smallCaption)
                    }
                }
                .frame(alignment: .leading)
            }
        }
    }
}

struct DoubleColumnGridView_Previews: PreviewProvider {
    static var previews: some View {
        DoubleColumnGridView()
    }
}
