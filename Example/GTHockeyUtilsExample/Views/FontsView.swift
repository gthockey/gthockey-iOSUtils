//
//  FontsView.swift
//  GTHockeyUtilsExample
//
//  Created by Caleb Rudnicki on 1/25/22.
//

import SwiftUI
import GTHockeyUtils

struct FontsView: View {
    
    var body: some View {
        ScrollView {
            VStack(alignment: .leading) {
                CustomText("Heading 1", font: .heading1)
                CustomText("Heading 2", font: .heading2)
                CustomText("Heading 3", font: .heading3)
                CustomText("Heading 4", font: .heading4)
            }
            .padding(.bottom, 20)
            
            VStack(alignment: .leading) {
                CustomText("Large Text", font: .largeText)
                CustomText("Medium Text", font: .mediumText)
                CustomText("Standard Caption", font: .standardCaption)
                CustomText("Normal Text", font: .normalText)
                CustomText("Small Text", font: .smallText)
                CustomText("Small Caption", font: .smallCaption)
                CustomText("Light Text", font: .lightText)
            }
            .padding(.bottom, 20)
            
            VStack(alignment: .leading) {
                CustomText("Large Text Semibold", font: .largeTextSemibold)
                CustomText("Medium Text Semibold", font: .mediumTextSemibold)
                CustomText("Caption Semibold", font: .captionSemibold)
                CustomText("Normal Text Semibold", font: .normalTextSemibold)
                CustomText("Small Text Semibold", font: .smallTextSemibold)
                CustomText("Small Caption Semibold", font: .smallCaptionSemibold)
            }
        }
    }
}

struct FontsView_Previews: PreviewProvider {
    static var previews: some View {
        FontsView()
    }
}
