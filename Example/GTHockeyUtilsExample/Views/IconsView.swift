//
//  IconsView.swift
//  GTHockeyUtilsExample
//
//  Created by Caleb Rudnicki on 2/18/22.
//

import SwiftUI
import GTHockeyUtils

struct IconsView: View {
    var body: some View {
        VStack {
            Text("Buzz Icon")
            Image.buzz
                .resizable()
                .frame(width: 100, height: 100)
        }
    }
}

struct IconsView_Previews: PreviewProvider {
    static var previews: some View {
        IconsView()
    }
}
