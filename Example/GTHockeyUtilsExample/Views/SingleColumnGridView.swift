//
//  SingleColumnGridView.swift
//  GTHockeyUtilsExample
//
//  Created by Caleb Rudnicki on 2/18/22.
//

import SwiftUI
import GTHockeyUtils

struct SingleColumnGridView: View {
    
    private let layout = [
        GridItem(.flexible()),
    ]
    
    var body: some View {
        ScrollView {
            LazyVGrid(columns: layout, spacing: 20) {
                CustomGridCell() {
                    Text("Simple with text")
                }
                
                CustomGridCell() {
                    VStack {
                        Text("Simple with text")
                        Text("Simple with text")
                    }
                }
                
                CustomGridCell() {
                    HStack(alignment: .top, spacing: 10) {
                        Image.buzz
                            .resizable()
                            .scaledToFit()
                            .frame(width: 120, height: 100)
                        VStack(alignment: .leading, spacing: 5) {
                            CustomText("Fall Recap - Jackets get ready for Nationals Push", font: .normalTextSemibold)
                                .lineLimit(3)
                            CustomText("Jan 4, 2022", font: .smallCaption)
                        }
                    }
                    .frame(alignment: .leading)
                }
                
                CustomGridCell(horizontalAlignment: .leading) {
                    Image.buzz
                        .resizable()
                        .scaledToFit()
                        .frame(width: 120, height: 100)
                }
                
                CustomGridCell() {
                    Image.buzz
                        .resizable()
                        .scaledToFit()
                        .frame(width: 120, height: 100)
                }
                
                CustomGridCell(horizontalAlignment: .trailing) {
                    Image.buzz
                        .resizable()
                        .scaledToFit()
                        .frame(width: 120, height: 100)
                }
            }
        }
        .padding(.horizontal, 20)
    }
}

struct SingleColumnGridView_Previews: PreviewProvider {
    static var previews: some View {
        SingleColumnGridView()
    }
}
