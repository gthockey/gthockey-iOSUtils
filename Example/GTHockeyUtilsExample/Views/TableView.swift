//
//  TableView.swift
//  GTHockeyUtilsExample
//
//  Created by Caleb Rudnicki on 1/27/22.
//

import SwiftUI
import GTHockeyUtils

struct TableView: View {
    
    var body: some View {
        List {
            SingleTableHeader("Single Label Cells")
            ForEach((1...15), id: \.self) {
                SingleLabelTableCell(label: "Single Label Table Cell \($0)")
            }

            DoubleTableHeader(label1: "Double", label2: "Table Headers")
            ForEach((1...15), id: \.self) {
                DoubleLabelTableCell(label1: "#\($0)", label2: "Double Label Table Cell \($0)")
            }
        }
        .listStyle(.plain)
        .background(Color.background)
    }
    
}

struct TableView_Previews: PreviewProvider {
    static var previews: some View {
        TableView()
    }
}
