# GTHockeyUtils

This package is a utility framework to unify code, components, and functionality across GT Hockey's iOS client applications.

## Installation
You can install `GTHockeyUtils` using Swift Package Manager using the following:
```
dependencies: [
    .package(url: "https://gitlab.com/gthockey/gthockey-iOSUtils.git", .upToNextMajor(from: "1.0.0"))
]
```

#### Dependencies
Currently, this package does not depend on any other packages, frameworks, or libraries.

## Features
There are various features that are included in `GTHockeyUtils`. None of them are required, but refer to the section of your choice to see how to properly use each feature.

#### Fonts
There is no need to import any custom font in the client app because all of the fonts are wrapped in this package. GT Hockey client apps use [Oswald](https://fonts.google.com/specimen/Oswald) as the custom font.

To use the custom fonts on `Text` elements, simple use the `.font()` descriptor like so:
```
Text("Large Text")
    .font(.largeText)
```

#### Colors
This package also handles any custom colors needed in GT Hockey client applications. Some of the colors are dark mode dependent and some are constant regarldless of the screen mode. To use the colors, use them just like any other `Color` in the app:
```
Rectangle()
    .fill(Color.gold)
```

If you would rather use `UIColor`, you can also use a color as follows:
```
UIColor.gold!
```

## Contributing
This package has its own example application to use and test its features on smaller scale. If you want to add to the package, make sure to test your work in the example application first -> `Example/GTHockeyUtilsExample/`

Once you are confident in your additions and are ready to update the package follow these steps to publish your changes:
1. Navigate to `GTHockeyUtils` in command line and run the following two commands to unsure the package can build and passes all tests. You can proceed if both steps complete successfully.
```
$ swift build
$ swift test
```
2. Add, commit, and push your changes
```
$ git add .
$ git commit -m "[your commit message]"
$ git push
```
3. Tag your commit
```
$ git tag <new version>
```
4. Push your changes to that specific tag
```
$ git push origin <new version>
```
