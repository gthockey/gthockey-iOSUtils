//
//  GTHockeyUtils+Image.swift
//  
//
//  Created by Caleb Rudnicki on 2/18/22.
//

import Foundation
import SwiftUI

public extension Image {
    
    /// Image of Buzz with a hockey stick
    static let buzz = Image("Buzz", bundle: .module)

}
