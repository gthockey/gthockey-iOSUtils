//
//  GTHockeyUtils+String.swift
//  
//
//  Created by Caleb Rudnicki on 2/15/22.
//

import Foundation

public extension String {
    
    /// A `Date` representation of a string
    var date: Date {
        let formatter = DateFormatter()
        formatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
        return formatter.date(from: self)!
    }
    
    /// Converts a string into an `NSAttributedString` that renders HTML
    var htmlToAttributedString: NSAttributedString? {
        guard let data = data(using: .utf8) else { return NSAttributedString() }
        do {
            return try NSAttributedString(data: data,
                                          options: [.documentType: NSAttributedString.DocumentType.html,
                                                    .characterEncoding:String.Encoding.utf8.rawValue],
                                          documentAttributes: nil)
        } catch {
            return NSAttributedString()
        }
    }
    
}
