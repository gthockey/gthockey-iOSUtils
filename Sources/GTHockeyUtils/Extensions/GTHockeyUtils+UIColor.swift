//
//  GTHockeyUtils+UIColor.swift
//  
//
//  Created by Caleb Rudnicki on 2/8/22.
//

import Foundation
import UIKit.UIColor

public extension UIColor {
    
    /// UIColor to be used as the background color for the app
    static var background = UIColor(named: "Background", in: .module, compatibleWith: nil)
    
    /// UIColor to be used as a section background color
    static var secondaryBackground = UIColor(named: "SecondaryBackground", in: .module, compatibleWith: nil)
    
    /// UIColor to be used for all text elements
    static var text = UIColor(named: "Text", in: .module, compatibleWith: nil)
    
    /// UIColor replicating the team gold color
    static var gold = UIColor(named: "Gold", in: .module, compatibleWith: nil)
    
    /// UIColor replicating the team navy color
    static var navy = UIColor(named: "Navy", in: .module, compatibleWith: nil)
    
    /// UIColor to be used as an accent color
    static var powder = UIColor(named: "Powder", in: .module, compatibleWith: nil)
    
    /// UIColor to be used for all destructive elements
    static var destructive = UIColor(named: "Destructive", in: .module, compatibleWith: nil)
    
}
