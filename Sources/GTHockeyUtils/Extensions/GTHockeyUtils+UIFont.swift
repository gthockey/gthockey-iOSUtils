//
//  GTHockeyUtils+UIFont.swift
//  
//
//  Created by Caleb Rudnicki on 2/21/22.
//

import Foundation
import UIKit.UIFont

public extension UIFont {
    
    /// Font: Oswald-Bold
    /// Size: 80
    static var heading1: UIFont {
        if !FontLoader.fontsHaveBeenLoaded { FontLoader.blast(bundle: .module) }
        return UIFont(name: "Oswald-Bold", size: 80)!
    }

    /// Font: Oswald-Bold
    /// Size: 60
    static var heading2: UIFont {
        if !FontLoader.fontsHaveBeenLoaded { FontLoader.blast(bundle: .module) }
        return UIFont(name: "Oswald-Bold", size: 60)!
    }
    
    /// Font: Oswald-Bold
    /// Size: 30
    static var heading3: UIFont {
        if !FontLoader.fontsHaveBeenLoaded { FontLoader.blast(bundle: .module) }
        return UIFont(name: "Oswald-Bold", size: 30)!
    }
    
    /// Font: Oswald-Bold
    /// Size: 24
    static var heading4: UIFont {
        if !FontLoader.fontsHaveBeenLoaded { FontLoader.blast(bundle: .module) }
        return UIFont(name: "Oswald-Bold", size: 24)!
    }
    
    /// Font: Oswald-Regular
    /// Size: 24
    static var largeText: UIFont {
        if !FontLoader.fontsHaveBeenLoaded { FontLoader.blast(bundle: .module) }
        return UIFont(name: "Oswald-Regular", size: 24)!
    }
    
    /// Font: Oswald-Regular
    /// Size: 20
    static var mediumText: UIFont {
        if !FontLoader.fontsHaveBeenLoaded { FontLoader.blast(bundle: .module) }
        return UIFont(name: "Oswald-Regular", size: 20)!
    }
    
    /// Font: Oswald-Regular
    /// Size: 18
    static var standardCaption: UIFont {
        if !FontLoader.fontsHaveBeenLoaded { FontLoader.blast(bundle: .module) }
        return UIFont(name: "Oswald-Regular", size: 18)!
    }
    
    /// Font: Oswald-Regular
    /// Size: 16
    static var normalText: UIFont {
        if !FontLoader.fontsHaveBeenLoaded { FontLoader.blast(bundle: .module) }
        return UIFont(name: "Oswald-Regular", size: 16)!
    }
    
    /// Font: Oswald-Light
    /// Size: 16
    static var lightText: UIFont {
        if !FontLoader.fontsHaveBeenLoaded { FontLoader.blast(bundle: .module) }
        return UIFont(name: "Oswald-Light", size: 16)!
    }
    
    /// Font: Oswald-Regular
    /// Size: 12
    static var smallText: UIFont {
        if !FontLoader.fontsHaveBeenLoaded { FontLoader.blast(bundle: .module) }
        return UIFont(name: "Oswald-Regular", size: 12)!
    }
    
    /// Font: Oswald-Regular
    /// Size: 12
    static var smallCaption: UIFont {
        if !FontLoader.fontsHaveBeenLoaded { FontLoader.blast(bundle: .module) }
        return UIFont(name: "Oswald-Regular", size: 12)!
    }
    
    /// Font: Oswald-SemiBold
    /// Size: 24
    static var largeTextSemibold: UIFont {
        if !FontLoader.fontsHaveBeenLoaded { FontLoader.blast(bundle: .module) }
        return UIFont(name: "Oswald-SemiBold", size: 24)!
    }
    
    /// Font: Oswald-SemiBold
    /// Size: 20
    static var mediumTextSemibold: UIFont {
        if !FontLoader.fontsHaveBeenLoaded { FontLoader.blast(bundle: .module) }
        return UIFont(name: "Oswald-SemiBold", size: 20)!
    }
    
    /// Font: Oswald-SemiBold
    /// Size: 18
    static var captionSemibold: UIFont {
        if !FontLoader.fontsHaveBeenLoaded { FontLoader.blast(bundle: .module) }
        return UIFont(name: "Oswald-SemiBold", size: 18)!
    }
    
    /// Font: Oswald-SemiBold
    /// Size: 16
    static var normalTextSemibold: UIFont {
        if !FontLoader.fontsHaveBeenLoaded { FontLoader.blast(bundle: .module) }
        return UIFont(name: "Oswald-SemiBold", size: 16)!
    }
    
    /// Font: Oswald-SemiBold
    /// Size: 12
    static var smallTextSemibold: UIFont {
        if !FontLoader.fontsHaveBeenLoaded { FontLoader.blast(bundle: .module) }
        return UIFont(name: "Oswald-SemiBold", size: 12)!
    }
    
    /// Font: Oswald-SemiBold
    /// Size: 12
    static var smallCaptionSemibold: UIFont {
        if !FontLoader.fontsHaveBeenLoaded { FontLoader.blast(bundle: .module) }
        return UIFont(name: "Oswald-SemiBold", size: 12)!
    }

}
