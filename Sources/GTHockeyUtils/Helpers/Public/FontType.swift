//
//  FontType.swift
//  
//
//  Created by Caleb Rudnicki on 2/21/22.
//

import Foundation
import SwiftUI
import UIKit.UIFont

/// A custom class to help move back and forth between `Font` and `UIFont` types.
public enum FontType: Equatable {
    
    case heading1
    case heading2
    case heading3
    case heading4
    case largeText
    case mediumText
    case standardCaption
    case normalText
    case lightText
    case smallText
    case smallCaption
    case largeTextSemibold
    case mediumTextSemibold
    case captionSemibold
    case normalTextSemibold
    case smallTextSemibold
    case smallCaptionSemibold
    
    /// The associated `Font` type
    var font: Font {
        switch self {
        case .heading1: return .heading1
        case .heading2: return .heading2
        case .heading3: return .heading3
        case .heading4: return .heading4
        case .largeText: return .largeText
        case .mediumText: return .mediumText
        case .standardCaption: return .standardCaption
        case .normalText: return .normalText
        case .lightText: return .lightText
        case .smallText: return .smallText
        case .smallCaption: return .smallCaption
        case .largeTextSemibold: return .largeTextSemibold
        case .mediumTextSemibold: return .mediumTextSemibold
        case .captionSemibold: return .captionSemibold
        case .normalTextSemibold: return .normalTextSemibold
        case .smallTextSemibold: return .smallTextSemibold
        case .smallCaptionSemibold: return .smallCaptionSemibold
        }
    }
    
    /// The associated `UIFont` type
    var uiFont: UIFont {
        switch self {
        case .heading1: return .heading1
        case .heading2: return .heading2
        case .heading3: return .heading3
        case .heading4: return .heading4
        case .largeText: return .largeText
        case .mediumText: return .mediumText
        case .standardCaption: return .standardCaption
        case .normalText: return .normalText
        case .lightText: return .lightText
        case .smallText: return .smallText
        case .smallCaption: return .smallCaption
        case .largeTextSemibold: return .largeTextSemibold
        case .mediumTextSemibold: return .mediumTextSemibold
        case .captionSemibold: return .captionSemibold
        case .normalTextSemibold: return .normalTextSemibold
        case .smallTextSemibold: return .smallTextSemibold
        case .smallCaptionSemibold: return .smallCaptionSemibold
        }
    }
    
    /// A function to compare two `FontType` instances.
    ///
    /// Some types map to `Font` and `UIFont` types that have the same font name
    /// and class, so this function specifically calls out those pairs of font types
    /// and returns `false`, otherwise, it defaults to the built in equals function
    /// for all `Font` types.
    ///
    /// - Parameters:
    ///   - lhs: the first `FontType` to compare
    ///   - rhs: the second `FontType` to compare
    /// - Returns: A `Bool` value describing if the two font types are equal
    public static func ==(lhs: FontType, rhs: FontType) -> Bool {
        switch (lhs, rhs) {
        case (.normalText, .lightText), (.lightText, .normalText),
            (.smallText, .smallCaption), (.smallCaption, .smallText),
            (.smallTextSemibold, .smallCaptionSemibold), (.smallCaptionSemibold, .smallTextSemibold):
            return false
        default:
            return lhs.font == rhs.font
        }
    }

}
