//
//  NetworkingError.swift
//  
//
//  Created by Caleb Rudnicki on 2/8/22.
//

import Foundation

/// A custom error class to be used on the networking layer
public enum NetworkingError: Error {
    case invalidUrl
    case invalidData
    case parsingError
}
