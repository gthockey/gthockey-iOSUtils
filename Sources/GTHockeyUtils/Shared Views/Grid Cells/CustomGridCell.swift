//
//  CustomGridCell.swift
//  
//
//  Created by Caleb Rudnicki on 2/18/22.
//

import SwiftUI

/// This grid cell allows the content to be custom set with a `View` of your own
public struct CustomGridCell<Content: View>: View {
    
    private let horizontalAlignment: HorizontalAlignment
    private let content: Content
    
    public init(horizontalAlignment: HorizontalAlignment = .center,
                @ViewBuilder content: () -> Content) {
        self.horizontalAlignment = horizontalAlignment
        self.content = content()
    }
    
    public var body: some View {
        content
            .padding(10)
            .frame(maxWidth: .infinity, alignment: Alignment(horizontal: horizontalAlignment,
                                                             vertical: .center))
            .background(Color.secondaryBackground)
            .cornerRadius(5)
            .shadow(color: Color.text.opacity(0.1), radius: 2)
    }
}

//struct CustomGridCell_Previews: PreviewProvider {
//    static var previews: some View {
//        CustomGridCell()
//    }
//}
