//
//  ImageCarousel.swift
//  
//
//  Created by Caleb Rudnicki on 3/6/22.
//

import SwiftUI
import SDWebImageSwiftUI

/// A view that holds a horizontal carousel of images
public struct ImageCarousel: View {
    
    @State private var visibleImage = 0
    
    private let imageUrls: [URL]
    
    public init(imageUrls: [URL]) {
        self.imageUrls = imageUrls
    }
    
    public var body: some View {
        ZStack(alignment: .center) {
            TabView(selection: $visibleImage) {
                ForEach(imageUrls.indices) { i in
                    WebImage(url: imageUrls[i])
                        .resizable()
                        .scaledToFill()
                        .tag(i)
                }
            }
            .tabViewStyle(.page(indexDisplayMode: .automatic))
            
            HStack {
                // Left arrow
                Button(action: {
                    visibleImage = max(visibleImage - 1, 0)
                }) {
                    Image(systemName: "arrowtriangle.backward.circle")
                }
                .font(.heading3)
                .background(Color.navy)
                .clipShape(Circle())
                .foregroundColor(.powder)
                .padding(.leading)
                
                Spacer()
                
                // Right arrow
                Button(action: {
                    visibleImage = min(visibleImage + 1, imageUrls.count - 1)
                }) {
                    Image(systemName: "arrowtriangle.right.circle")
                }
                .font(.heading3)
                .background(Color.navy)
                .clipShape(Circle())
                .foregroundColor(.powder)
                .padding(.trailing)
            }
        }
    }
}

//struct ImageCarousel_Previews: PreviewProvider {
//    static var previews: some View {
//        ImageCarousel()
//    }
//}
