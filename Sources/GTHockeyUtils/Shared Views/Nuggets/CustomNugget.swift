//
//  CustomNugget.swift
//  
//
//  Created by Caleb Rudnicki on 2/15/22.
//

import SwiftUI

/// This nugget allows the content to be custom set with a `View` of your own
public struct CustomNugget<Content: View>: View {
    
    private var title: String
    private let content: Content
    
    public init(title: String, @ViewBuilder content: () -> Content) {
        self.title = title
        self.content = content()
    }
    
    public var body: some View {
        VStack(alignment: .leading, spacing: 20) {
            CustomText(title, font: .captionSemibold)
                .frame(maxWidth: .infinity, alignment: .leading)
                .padding(.top, 5)
            content
                .padding(.bottom, 20)
        }
        .frame(maxWidth: .infinity)
        .padding(.horizontal, 10)
        .background(Color.secondaryBackground)
        .cornerRadius(5)
    }
}

//struct CustomNugget_Previews: PreviewProvider {
//    static var previews: some View {
//        CustomNugget()
//    }
//}
