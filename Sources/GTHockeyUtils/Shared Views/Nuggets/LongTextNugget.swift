//
//  LongTextNugget.swift
//  
//
//  Created by Caleb Rudnicki on 2/5/22.
//

import SwiftUI

public struct LongTextNugget: View {
    
    private let title: String
    private let content: String
    
    public init(title: String, content: String) {
        self.title = title
        self.content = content
    }
    
    public var body: some View {
        VStack(alignment: .leading) {
            CustomText(title, font: .captionSemibold)
                .frame(maxWidth: .infinity, alignment: .leading)
                .padding(.top, 5)
                .padding(.leading, 10)
            CustomText(content, font: .lightText)
                .frame(maxWidth: .infinity, alignment: .leading)
                .padding([.top, .leading], 10)
                .padding(.bottom, 30)
        }
        .frame(maxWidth: .infinity)
        .background(Color.secondaryBackground)
        .cornerRadius(5)
    }
}

struct LongTextNugget_Previews: PreviewProvider {
    static var previews: some View {
        LongTextNugget(title: "Title", content: "Content")
    }
}
