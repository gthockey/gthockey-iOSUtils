//
//  DoubleLabelTableCell.swift
//  
//
//  Created by Caleb Rudnicki on 1/27/22.
//

import SwiftUI

public struct DoubleLabelTableCell: View {
        
    private let label1: String
    private let label2: String
    
    public init(label1: String, label2: String) {
        self.label1 = label1
        self.label2 = label2
    }
    
    public var body: some View {
        HStack(spacing: .none) {
            Text(label1)
                .font(.normalText)
                .foregroundColor(.text)
                .frame(width: 35)
            Text(label2)
                .font(.normalText)
                .foregroundColor(.text)
        }
        .padding([.top, .bottom], 20)
        .listRowBackground(Color.background)
    }
    
}

struct DoubleLabelTableCell_Previews: PreviewProvider {
    static var previews: some View {
        DoubleLabelTableCell(label1: "Preview Label 1", label2: "Preview Label 2")
    }
}
