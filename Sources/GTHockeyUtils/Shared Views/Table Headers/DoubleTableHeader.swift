//
//  DoubleTableHeader.swift
//  
//
//  Created by Caleb Rudnicki on 2/22/22.
//

import SwiftUI

public struct DoubleTableHeader: View {
    
    private var label1: String
    private var label2: String
    
    public init(label1: String, label2: String) {
        self.label1 = label1
        self.label2 = label2
    }
    
    public var body: some View {
        if #available(iOS 15.0, *) {
            HStack {
                CustomText(label1, font: .mediumText)
                Spacer()
                CustomText(label2, font: .mediumText)
            }
            .padding(.bottom, 10)
            .padding(.top, 20)
            .listRowBackground(Color.background)
            .listRowSeparator(.hidden)
        } else {
            // Fallback on earlier versions
            HStack {
                CustomText(label1, font: .mediumText)
                Spacer()
                CustomText(label2, font: .mediumText)
            }
            .padding(.bottom, 10)
            .padding(.top, 20)
            .listRowBackground(Color.background)
        }
    }
}

struct DoubleTableHeader_Previews: PreviewProvider {
    static var previews: some View {
        DoubleTableHeader(label1: "Label 1", label2: "Label 2")
    }
}
