//
//  TextView.swift
//  
//
//  Created by Caleb Rudnicki on 2/21/22.
//

import Foundation
import SwiftUI
import UIKit

internal struct TextView: UIViewRepresentable {
 
    internal var text: String
    internal var font: UIFont
    internal var width: CGFloat
    
    @Binding internal var height: CGFloat
    
    private let textView = UITextView()
    private let attributes = [NSAttributedString.Key.font: UIFont.lightText,
                              NSAttributedString.Key.foregroundColor: UIColor.text]
 
    func makeUIView(context: Context) -> UITextView {
        let contentString = text.replacingOccurrences(of: "\n", with: "<br>")
        
        textView.isEditable = false
        textView.isSelectable = true
        textView.isScrollEnabled = false
        textView.isUserInteractionEnabled = true
        textView.backgroundColor = .clear
        textView.translatesAutoresizingMaskIntoConstraints = false
        textView.widthAnchor.constraint(equalToConstant: width).isActive = true
        
        DispatchQueue.main.async {
            // Setting the attributed string of the text field
            let attributedString = contentString.htmlToAttributedString?.mutableCopy() as! NSMutableAttributedString
            attributedString.addAttributes(attributes as [NSAttributedString.Key : Any],
                                           range: NSRange(location: 0, length: attributedString.length))
            textView.attributedText = attributedString
            
            // Calculating the height of the text field and piping it back up to be set at the top level
            self.height = textView.sizeThatFits(CGSize(width: width, height: .infinity)).height
        }
        
        return textView
    }
 
    func updateUIView(_ uiView: UITextView, context: Context) {
        let contentString = text.replacingOccurrences(of: "\n", with: "<br>")
        
        DispatchQueue.main.async {
            // Setting the attributed string of the text field
            let attributedString = contentString.htmlToAttributedString?.mutableCopy() as! NSMutableAttributedString
            attributedString.addAttributes(attributes as [NSAttributedString.Key : Any],
                                           range: NSRange(location: 0, length: attributedString.length))
            textView.attributedText = attributedString
            
            // Calculating the height of the text field and piping it back up to be set at the top level
            self.height = textView.sizeThatFits(CGSize(width: width, height: .infinity)).height
        }
    }
    
}
