//
//  CustomText.swift
//  
//
//  Created by Caleb Rudnicki on 2/18/22.
//

import SwiftUI

/// This class allows the a `Text` element to be created with all of the necessary edits to match designs.
public struct CustomText: View {
    
    private let text: String
    private let font: FontType
    
    private var isTextCaseUppercase: Bool {
        font == FontType.standardCaption ||
        font == FontType.smallCaption ||
        font == FontType.captionSemibold ||
        font == FontType.smallCaptionSemibold
    }
    private var isTextSpacingEnlarged: Bool {
        font == FontType.lightText ||
        font == FontType.smallText ||
        font == FontType.smallTextSemibold
    }
    
    public init(_ text: String, font: FontType) {
        self.text = text
        self.font = font
    }
    
    public var body: some View {
        if isTextCaseUppercase && isTextSpacingEnlarged {
            Text(text)
                .foregroundColor(.text)
                .font(font.font)
                .tracking(2)
                .textCase(.uppercase)
        } else if isTextCaseUppercase {
            Text(text)
                .foregroundColor(.text)
                .font(font.font)
                .textCase(.uppercase)
        } else if isTextSpacingEnlarged {
            Text(text)
                .foregroundColor(.text)
                .font(font.font)
                .tracking(2)
        } else {
            Text(text)
                .foregroundColor(.text)
                .font(font.font)
        }
    }
    
}

//struct CustomText_Previews: PreviewProvider {
//    static var previews: some View {
//        CustomText()
//    }
//}
