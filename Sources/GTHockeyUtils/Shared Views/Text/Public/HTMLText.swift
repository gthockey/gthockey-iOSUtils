//
//  HTMLText.swift
//  
//
//  Created by Caleb Rudnicki on 2/20/22.
//

import Foundation
import SwiftUI

public struct HTMLText: View {
    
    private let text: String
    private let font: FontType
    
    @Binding private var height: CGFloat
    
    public init(_ text: String, font: FontType, height: Binding<CGFloat>) {
        self.text = text
        self.font = font
        self._height = height
    }
    
    public var body: some View {
        GeometryReader { geo in
            TextView(text: text, font: font.uiFont, width: geo.size.width, height: $height)
        }
    }
}

//struct HTMLText_Previews: PreviewProvider {
//    static var previews: some View {
//        HTMLText()
//    }
//}
